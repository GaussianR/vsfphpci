<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->__load_lang_files();
    $this->load->model('NewsModel', 'news', TRUE);
  }

  private function __load_lang_files()
  {
    $this->lang->load('news');
    $this->lang->load('page');
  }
  
  private function __render($news, $category, $term, $rtotal)
  {
    if ($news != NULL)
    {
      $config = $this->config->item('news_pag_config');
      
      $config['total_rows'] = $rtotal;
      $config['base_url'] = base_url('search/' . $category . '/' . $term);
      
      $this->load->library('pagination');
      $this->pagination->initialize($config);
    }
    
    $params['search_term'] = $term;
    $params['search_category'] = $category; 
    $params['title'] = $this->config->item('page_title');
    
    $params['view'] = $this->load->view(
            $news == NULL ? 'news/notfound' : 'news/list', 
            $news == NULL ? NULL : array('news' => $news), TRUE);

    $this->load->view('page/body', $params);
  }

  public function author()
  {
    $offset = $this->uri->segment(4);
    $offset = $offset == NULL ? 0 : intval($offset);
    
    $author = urldecode($this->uri->segment(3));
    if($author == NULL)
      redirect('');
    
    $this->__render($this->news->GetByAuthor($author, (object)array(
        'count' => $this->config->item('news_pag_config')['per_page'],
        'offset' => $offset,
    )), 'author', $author, $this->news->GetByAuthorCount($author));
  }

  public function keywords()
  {
    $offset = $this->uri->segment(4);
    $offset = $offset == NULL ? 0 : intval($offset);
    
    $keywords = urldecode($this->uri->segment(3));
    if($keywords == NULL)
      redirect('');

    $this->__render($this->news->GetByKeywords($keywords, (object)array(
        'count' => $this->config->item('news_pag_config')['per_page'],
        'offset' => $offset,
    )), 'keywords', $keywords, $this->news->GetByKeywordsCount($keywords));
  }

  public function pattern()
  {
    $offset = $this->uri->segment(4);
    $offset = $offset == NULL ? 0 : intval($offset);
    
    $pattern = urldecode($this->uri->segment(3));
    if($pattern == NULL)
      redirect('');
    
    $this->__render($this->news->GetByPattern($pattern, (object)array(
        'count' => $this->config->item('news_pag_config')['per_page'],
        'offset' => $offset,
    )), 'pattern', $pattern, $this->news->GetByPatternCount($pattern));
  }

}
