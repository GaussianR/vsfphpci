<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->__load_lang_files();
    $this->load->model('NewsModel', 'news', TRUE);
  }

  private function __load_lang_files()
  {
    $this->lang->load('news');
    $this->lang->load('page');
  }
  
  /**
   * 
   */
  public function index()
  {
    $offset = $this->uri->segment(3);
    $offset = $offset == NULL ? 0 : intval($offset);
    $pag_config = $this->config->item('news_pag_config');
    $news = $this->news->GetLasts((object)array(
        'offset' => $offset,
        'count' => $pag_config['per_page']
    ));

    if ($news != NULL)
    {
      $pag_config['total_rows'] = $this->news->GetCount();
      $pag_config['base_url'] = base_url('main/index');
      
      $this->load->library('pagination');
      $this->pagination->initialize($pag_config);
    }

    $params['title'] = $this->config->item('page_title');
    $params['view'] = $this->load->view(
            $news == NULL ? 'news/notfound' : 'news/list', 
            $news == NULL ? NULL : array('news' => $news), TRUE);

    $this->load->view('page/body', $params);
  }
}
