<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->__load_lang_files();
    $this->load->model('NewsModel', 'news', TRUE);
  }
  
  private function __load_lang_files()
  {
    $this->lang->load('news');
    $this->lang->load('page');
  }

  private function __validate_edit_form()
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Titulo', 'required');
    $this->form_validation->set_rules('author', 'Autor', 'required');
    $this->form_validation->set_rules('summary', 'Resumen', 'required');
    $this->form_validation->set_rules('image', 'Imagen', 'required');
    $this->form_validation->set_rules('keywords', 'Palabras clave', 'required');
    
    return $this->form_validation->run();
  }
  
  private function __repopulate_edit_form(&$new)
  {
    $new->image = $this->input->post('image');
    $new->title = $this->input->post('title');
    $new->author = $this->input->post('author');
    $new->summary = $this->input->post('summary');
    $new->content = $this->input->post('content');
    $new->keywords = $this->input->post('keywords');
  }
  
  private function __dependencies_edit_form(&$params)
  {
    $params['js_dependencies'] = array(
        base_url('assets/js/tagsinput.js'),
        'http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.js',
        base_url('assets/js/summernote-es-ES.js'),
        base_url('assets/js/edit.js'),
    );

    $params['css_dependencies'] = array(
        base_url('assets/css/tagsinput.css'),
        'http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.css',
    );
  }
  
  public function view()
  {
    $id = intval($this->uri->segment(3));
    $new = $this->news->Get($id);

    $params['title'] = $new == NULL ?
    $this->config->item('page_title') : $new->title;
    $params['view'] = $this->load->view($new == NULL ? 
             'news/notfound' : 'news/detail', $new, TRUE);

    $this->load->view('page/body', $params);
  }

  public function edit()
  {
    $new = NULL;
    $id = intval($this->uri->segment(3));
    $rmethod = $this->input->server('REQUEST_METHOD');

    if ($rmethod == 'POST')
    {
      if ($this->__validate_edit_form())
      {
        $this->news->Update($id,
        $this->input->post('title'),
        $this->input->post('author'),
        $this->input->post('summary'),
        $this->input->post('content'),
        $this->input->post('keywords'),
        $this->input->post('image'));

        redirect('news/view/' . $id);
      }
      else if(($new = $this->news->Get($id)) != NULL)
      {
        $this->__repopulate_edit_form($new);
      }
    }
    else if ($rmethod == 'GET')
    {
      $new = $this->news->Get($id);
    }

    $params['title'] = $new == NULL ?
    $this->config->item('page_title') : $new->title;
    $params['view'] = $this->load->view($new == NULL ? 
             'news/notfound' : 'news/edit', $new, TRUE);
    
    $this->__dependencies_edit_form($params);
    $this->load->view('page/body', $params);
  }

  public function create()
  {
    $rmethod = $this->input->server('REQUEST_METHOD');
    $new = new stdClass();
    $new->id = -1;
    
    if ($rmethod == 'POST')
    {
      if ($this->__validate_edit_form())
      {
        $id = $this->news->Create(
        $this->input->post('title'),
        $this->input->post('author'),
        $this->input->post('summary'),
        $this->input->post('content'),
        $this->input->post('keywords'),
        $this->input->post('image'));

        redirect('news/view/' . $id);
      }
      else
      {
        $this->__repopulate_edit_form($new);
      }
    }
    else if ($rmethod == 'GET')
    {
      $new->title = '';
      $new->author = '';
      $new->summary = '';
      $new->content = '';
      $new->keywords = '';
      $new->image = $this->config->item('news_default_image');
    }
    
    $params['section'] = 'news/create';
    $params['title'] = $this->config->item('page_title');
    $params['view'] = $this->load->view('news/edit', $new, TRUE);

    $this->__dependencies_edit_form($params);
    $this->load->view('page/body', $params);
  }

}
