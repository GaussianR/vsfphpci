<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- News List -->
<?php 
foreach ($news as $new)
{
  $this->load->view('news/summary', $new);
}
?>

<!-- Pagination -->
<div class="row">
  <div class="col-md-12 text-center">
    <nav>
      <?= $this->pagination->create_links() ?>
    </nav>
  </div>
</div>


