<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<form class="form-horizontal" action="<?= base_url($id == -1 ? 'news/create' : 'news/edit/' . $id) ?>" method="POST" role="form">
  
  <?php if (validation_errors()): ?>
    <!-- Error Validation -->
    <div class="alert alert-danger">
      <?= validation_errors() ?>
    </div>
  <?php endif; ?>
  
  <div class="row">
    
     <!-- Image dialog -->
    <?= $this->load->view('utils/modals/image', array('id' => 'image-modal'), TRUE) ?>
    
    <!-- Image container -->
    <div class="col-md-4 text-center">
      <div class="img-hover">
        <img id="image-preview" class="img-thumbnail img-hover-image" src="<?= $image ?>" alt="<?= $title ?>" width="240" height="240">
        <input id="new-image" type="hidden" name="image" value="<?= $image ?>">

        <div class="img-hover-body" data-toggle="modal" data-target="#image-modal">
          <h1><span class="glyphicon glyphicon-option-horizontal"></span></h1>
        </div>
      </div>
    </div>

    <div class="col-md-8">

      <!-- Title -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-title"><?= lang('news_edit_title') ?>:</label>
        <div class="col-sm-10">
          <input class="form-control" value="<?= $title ?>" id="new-title" name="title" placeholder="Introduce un título" required>
        </div>
      </div>
      
      <!-- Author -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-author"><?= lang('news_edit_author') ?>:</label>
        <div class="col-sm-10">
          <input class="form-control" value="<?= $author ?>" id="new-author" name="author" placeholder="Introduce un autor" required>
        </div>
      </div>
      
      <!-- Summary -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-summary"><?= lang('news_edit_summary') ?>:</label>
        <div class="col-sm-10"> 
          <textarea class="form-control" id="new-summary" name="summary" required></textarea>
        </div>
      </div>

      <!-- Keywords -->
      <div class="form-group">
        <label class="control-label col-sm-2" for="new-keywords"><?= lang('news_edit_keywords') ?>:</label>
        <div class="col-sm-10">
          <input class="form-control summernote" value="<?= $keywords ?>" id="new-keywords" name="keywords" data-role="tagsinput" required>
        </div>
      </div>
    </div>
  </div>
  <hr>

  <!-- Content -->
  <div class="row">
    <div class="panel-body"> 
      <textarea class="form-control summernote" id="new-content" name="content"></textarea>
    </div>
  </div>

  <div class="form-group panel-body pull-right"> 
    <button type="submit" class="btn btn-default"><?= lang($id == -1 ? 'news_edit_create' : 'news_edit_modify') ?></button>
  </div>
</form>

<script type="text/javascript">
  function edit_postinit()
  {
    $('#new-summary').summernote('code', <?= json_encode($summary) ?>);
    $('#new-content').summernote('code', <?= json_encode($content) ?>);
  }
</script>