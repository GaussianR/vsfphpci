<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>


<h1><?= $title ?>
  <div class="panel-body">
    <!-- New creation date -->
    <small>
      <?= lang('news_created_author') ?> <a href="<?= base_url('search/author/' . $author) ?>" title="<?= lang('news_from_author').$author ?>" alt="<?= lang('news_from_author').$author ?>" rel="author"><?= $author ?></a> el <b><?= date('j M Y', strtotime($created)) ?></b>
      <!-- Edit button -->
      <a data-toggle="tooltip" title="<?= lang('news_edit') ?>" alt="<?= lang('news_edit') ?>" class="pull-right" href="<?= base_url('news/edit/' . $id) ?>"><i class="glyphicon glyphicon-pencil"></i></a>
    </small>
  </div>
</h1>
<div class="panel-body">
  <?= $content ?>
</div>

<?php if ($updated != NULL): ?>
  <!-- Modification date -->
  <h3 class="text-right">
    <small><?= lang('news_updated_date') ?> <b><?= date('j M Y', strtotime($updated)) ?></b> <?= lang('news_updated_hour') ?> <b><?= date('H:i', strtotime($updated)) ?></b></small>
  </h3>
<?php endif; ?>

<hr>

<div class="panel-body">
  <p><?= lang('news_keywords') ?> 
    <?php foreach (explode(' ', $keywords) as $keyword): ?>
      &nbsp;<a href="<?= base_url('search/keywords/' . $keyword) ?>" alt="<?= $keyword ?>"><span class="label label-default"><span class="glyphicon glyphicon-tag"></span> <?= $keyword ?></span></a>
    <?php endforeach; ?>
  </p>
</div>