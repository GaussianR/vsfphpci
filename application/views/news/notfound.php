<?php
  
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="alert alert-danger">
  <?= lang('news_notfound') ?>
</div>