<?php
  
defined('BASEPATH') OR exit('No direct script access allowed');
$link = base_url('news/view/' . $id);
?>

<div class="row">
  <div class="col-sm-3">
    <a href="<?= $link ?>">
      <img class="img-thumbnail" src="<?= $image ?>" alt="<?= $title ?>" width="240" height="240">							
    </a>
  </div>
  <div class="col-sm-9">

    <!-- New title -->
    <h2>
      <a href="<?= $link ?>"><?= $title ?></a>
    </h2>


    <p class="post-meta">
      <!-- New creation date -->
      <?= lang('news_created_author') ?> <a href="<?= base_url('search/author/' . $author) ?>" title="<?= lang('news_from_author').$author ?>" alt="<?= lang('news_from_author').$author ?>" rel="author"><?= $author ?></a> <?= lang('news_created_date') ?> <b><?= date('j M Y', strtotime($created)) ?></b> 

      <!-- Edit button -->
      <a data-toggle="tooltip" title="<?= lang('news_edit') ?>" alt="<?= lang('news_edit') ?>" class="pull-right" href="<?= base_url('news/edit/' . $id) ?>"><i class="glyphicon glyphicon-pencil"></i></a>  
    </p>
    <p></p>
    <?= $summary ?>
    <p></p>
    <div class="new-controls">
      <a href="<?= $link ?>" class="btn btn-primary pull-right"><span><?= lang('news_read_more') ?></span></a>
    </div>
  </div>
</div>
<hr>