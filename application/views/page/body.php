<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title><?= $title ?></title>
      <!-- Latest compiled and minified CSS of Boostrap -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>

      <!-- Optional theme for Bootstrap -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

      <script type="text/javascript">window.base_url = "<?= base_url() ?>";</script>

      <!-- Latest compiled and minified JavaScript of jQuery -->
      <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
      <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

      <!-- Latest compiled and minified JavaScript of Bootstrap -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

      <?php if (isset($css_dependencies)): ?>
        <!-- View CSS dependencies -->
        <?php foreach ($css_dependencies as $dependency): ?>
          <link rel="stylesheet" href="<?= $dependency ?>"/>
        <?php endforeach; ?>
      <?php endif; ?>


      <?php if (isset($js_dependencies)): ?>
        <!-- View JS dependencies -->
        <?php foreach ($js_dependencies as $dependency): ?>
          <script src="<?= $dependency ?>"></script>
        <?php endforeach; ?>
      <?php endif; ?>

      <!-- Application CSS File -->
      <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>"/>

      <!-- Application JS File -->
      <script src="<?= base_url('assets/js/main.js') ?>"></script>
  </head>

  <body>

    <!-- Main container -->
    <div class="container main-container">
      
      <!-- Navigation bar -->
      <?= $this->load->view('page/navigation', array(
          'section' => isset($section) ? $section : '',
          'search_category' => isset($search_category) ? $search_category : 'pattern',
          'search_term' => isset($search_term) ? $search_term : ''), TRUE) ?>
      
      <!-- Main panel -->
      <div class="panel panel-default container-panel">
        <div class="panel-body">
          <?= $view ?>
        </div>
        <div class="panel-footer text-left">
          <div class="col-xs-6">
            <?= lang('page_developed_by') ?> <a href="mailto:mario.sb93@gmail.com">Mario Salazar de Torres</a>
          </div>
          <div class="col-xs-6 text-right">
            <?= lang('news_page_generated') ?> <strong>{elapsed_time}</strong> . <?= (ENVIRONMENT === 'development') ? 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
          </div>
        </div>
      </div>

    </div>
  </body>
</html>