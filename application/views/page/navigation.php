<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

      <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-search-collapse" aria-expanded="false">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url() ?>"><?= lang('page_nav_home') ?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-search-collapse">
          <ul class="nav navbar-nav">
            <li <?= $section == 'news/create' ? 'class="active"' : ''?>>
              <a href="<?= base_url('/news/create') ?>"><?= lang('news_nav_create') ?></a>
            </li>
          </ul>

          <!-- Search box -->
          <?php
          $search_categories = array(
              'pattern'  => lang('news_search_pattern'),
              'author'   => lang('news_search_author'),
              'keywords' => lang('news_search_keywords'),
          );
          ?>
          <div id="search-form" class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <?= form_dropdown('name', $search_categories, $search_category, 'class="btn btn-default dropdown-toggle" data-toggle="dropdown"') ?>
              <div class="btn-group">
                <input type="text" class="form-control" placeholder="<?= lang('news_search_placeholder') ?>" value="<?= $search_term ?>">
                  <span class="search-clear glyphicon glyphicon-remove-circle" <?= $search_term != '' ? '' : 'style="display: none;"' ?>></span>
              </div>
            </div>
            <button type="submit" class="btn btn-default">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </div>

        </div><!-- /.navbar-collapse -->
      </nav>