<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div id="<?= $id ?>" class="modal in" aria-hidden="false" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Seleccionar imagen</h4>
          </div>
          <div class="modal-body">
            <div class="form-group note-group-select-from-files">
              <label>Seleccionar desde los archivos</label>
              <input class="note-image-input form-control" type="file" name="files" accept="image/*" max-size="2097152">
            </div>
            <div class="form-group" style="overflow:auto;">
              <label>URL de la imagen</label>
              <input class="note-image-url form-control col-md-12" type="text">
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-primary note-image-btn disabled" disabled="">Aplicar</button>
          </div>
        </div>
      </div>
    </div>