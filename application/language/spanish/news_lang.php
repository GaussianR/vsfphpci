<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['news_nav_create'] = 'Nueva noticia';

$lang['news_search_author'] = 'Autor';
$lang['news_search_pattern'] = 'Todos';
$lang['news_search_keywords'] = 'Palabras clave';
$lang['news_search_placeholder'] = 'Buscar términos...';

$lang['news_created_date'] = 'el';
$lang['news_updated_hour'] = 'a las';
$lang['news_created_author'] = 'Publicado por';
$lang['news_updated_date'] = 'Última modificación el';

$lang['news_from_author'] = 'Noticias de ';
$lang['news_keywords'] = 'Palabras clave:';

$lang['news_edit'] = 'Editar noticia';
$lang['news_read_more'] = 'Leer más';

$lang['news_notfound'] = 'No se ha encontrado ninguna noticia.';

$lang['news_edit_title'] = 'Título';
$lang['news_edit_author'] = 'Autor';
$lang['news_edit_summary'] = 'Resumen';
$lang['news_edit_keywords'] = 'Términos';
$lang['news_edit_create'] = 'Añadir';
$lang['news_edit_modify'] = 'Modificar';

$lang['news_edit_title_placeholder'] = 'Introduce un título';
$lang['news_edit_author_placeholder'] = 'Introduce un autor';