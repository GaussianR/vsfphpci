<?php

class NewsModel extends CI_Model
{

  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
    $this->load->database();
  }

  private function __set_params($limits, $sorting)
  {
    if ($limits != NULL)
    {
      if ($limits->offset < 0)
      {
        return FALSE;
      }

      $this->db->limit($limits->count, $limits->offset);
    }
    
    if($sorting == NULL)
      $sorting = $this->config->item('news_default_sorting');
    
    $this->db->order_by($sorting->field, $sorting->order);
    
    return TRUE;
  }
  
  function GetCount()
  {
    return $this->db->count_all('news');
  }

  function GetLasts($limits = NULL, $sorting = NULL)
  {
    if(!$this->__set_params($limits, $sorting))
      return NULL;
            
    $query = $this->db->get('news');
    return $query->result();
  }

  function GetByAuthorCount($author)
  {
    $this->db->like('author', $author);
    $this->db->from('news');

    return $this->db->count_all_results();
  }

  function GetByAuthor($author, $limits = NULL, $sorting = NULL)
  {
    if(!$this->__set_params($limits, $sorting))
      return NULL;

    $this->db->like('author', $author);
    $query = $this->db->get('news');
    return $query->result();
  }

  function GetByKeywordsCount($keywords)
  {
    $this->db->where('MATCH (keywords) AGAINST (' . 
    $this->db->escape($keywords) . ' IN BOOLEAN MODE)', NULL, FALSE);
    $this->db->from('news');

    return $this->db->count_all_results();
  }

  function GetByKeywords($keywords, $limits = NULL, $sorting = NULL)
  {
    if(!$this->__set_params($limits, $sorting))
      return NULL;
    
    $this->db->where('MATCH (keywords) AGAINST (' . 
    $this->db->escape($keywords) . ' IN BOOLEAN MODE)', NULL, FALSE);
    $query = $this->db->get('news');
    return $query->result();
  }

  function GetByPatternCount($pattern)
  {
    $this->db->like('author', $pattern);
    $this->db->or_where('MATCH (keywords) AGAINST (' . 
    $this->db->escape($pattern) . ' IN BOOLEAN MODE)', NULL, FALSE);
    $this->db->from('news');

    return $this->db->count_all_results();
  }
  
  function GetByPattern($pattern, $limits = NULL, $sorting = NULL)
  {
    if(!$this->__set_params($limits, $sorting))
      return NULL;

    $this->db->like('author', $pattern);
    $this->db->or_where('MATCH (keywords) AGAINST (' . 
    $this->db->escape($pattern) . ' IN BOOLEAN MODE)', NULL, FALSE);
    $query = $this->db->get('news');
    return $query->result();
  }

  function Exists($id)
  {
    $this->db->select('1');
    $this->db->where('id', $id);
    $query = $this->db->get('news');
    return $query->num_rows() > 0;
  }

  function Get($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get('news');
    return $query->last_row();
  }

  function Update($id, $title, $author, $summary, $content, $keywords, $image)
  {
    $this->db->where('id', $id);
    $this->db->set('title', $title);
    $this->db->set('image', $image);
    $this->db->set('author', $author);
    $this->db->set('summary', $summary);
    $this->db->set('content', $content);
    $this->db->set('keywords', $keywords);
    $this->db->set('updated', 'CURRENT_TIMESTAMP', FALSE);
    $this->db->update('news');

    return $this->db->affected_rows() == 1;
  }

  function Create($title, $author, $summary, $content, $keywords, $image)
  {
    $this->db->insert('news', array(
        'title'    => $title,
        'author'   => $author,
        'summary'  => $summary,
        'content'  => $content,
        'keywords' => $keywords,
        'image'    => $image,
    ));

    return $this->db->insert_id();
  }
}
