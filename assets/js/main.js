function search()
{
  var input = $('#search-form input').val();
  var category = $('#search-form select').val();

  if (input != '')
    window.location.href = window.base_url + "search/" + category + "/" + input;
}

$(document).ready(function()
{
  $('#search-form button').click(function ()
  {
    search();
  });

  $('#search-form input').keypress(function (e)
  {
    if ($(this).val() != '')
      $('#search-form .search-clear').show();

    if (e.which == 13)
    {
      search();
    }
  });

  $('#search-form .search-clear').click(function ()
  {
    $(this).hide();
    var input = $('#search-form input').val('');
    var category = $('#search-form select').val('pattern');
  });

});
